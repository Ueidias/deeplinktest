import { NextResponse } from "next/server";

const BUNDLE_ID = "au.com.testbet.ios.dev";
const TEAM_ID = "8944J88AQW";

export async function GET() {
  const response = NextResponse.json(
    {
      applinks: {
        apps: [],
        details: [
          {
            appID: `${TEAM_ID}.${BUNDLE_ID}`,
            paths: ["*"],
          },
        ],
      },
    },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  return response;
}
